﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication3.Models;
using WebPage.Models;

namespace WebApplication3.Controllers
{
	public class HomeController : Controller
	{
		private readonly ILogger<HomeController> _logger;

		public HomeController(ILogger<HomeController> logger)
		{
			_logger = logger;
		}

		public IActionResult Index()
		{
			return View();
		}

		public IActionResult Login()
		{
			return View();
		}
		public IActionResult Register()
		{
			return View();
		}


		[HttpPost]
		public IActionResult GetDetails()
		{
			string tx = "Register";

			UserModel umodel = new UserModel();
			umodel.FirstName = HttpContext.Request.Form["Fname"].ToString();
			umodel.SecondName = HttpContext.Request.Form["Sname"].ToString();
			umodel.Email = HttpContext.Request.Form["email"].ToString();
			umodel.DOB = Convert.ToDateTime(HttpContext.Request.Form["Dob"]);
			umodel.Gender = HttpContext.Request.Form["gen"].ToString();
			umodel.Username = HttpContext.Request.Form["usn"].ToString();
			umodel.Password = HttpContext.Request.Form["pwd"].ToString();
			string CPassword = HttpContext.Request.Form["cpwd"].ToString();

			if (HttpContext.Request.Form["cpwd"].ToString() == HttpContext.Request.Form["pwd"].ToString())
			{
				if (umodel.CheckLogin())
				{
					ViewData["Message"] = "Username already exist";
				}
				else
				{
					int result = umodel.SaveDetails();
					if (result > 0)
					{
						ViewData["Message"] = "Registration Successfully";
						tx = "Login";

					}
					else
					{
						ViewData["Message"] = "Something Went Wrong";
					}
				}



			}
			else
			{
				ViewData["Message"] = "Password Do Not Match";

			}
			return View(tx);
		}

		[HttpPost]
		public IActionResult SendDetails()
		{

			UserModel umodel = new UserModel();

			umodel.Username = HttpContext.Request.Form["username"].ToString();
			umodel.Password = HttpContext.Request.Form["password"].ToString();
			string tx = "Login";
			if (umodel.CheckLogin())
			{
				ViewData["Fullname"] = umodel.FirstName + " " + umodel.SecondName;
				ViewData["Email"] = umodel.Email;

				tx = "DashBoard";

			}
			else
			{
				ViewData["Message"] = "Invalid Username or Password";

			}
			return View(tx);

		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}
